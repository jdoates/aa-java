# README #

This repository is for Algorithms and Analysis Assignment 2

### How do I get set up? ###

to compile on Linux:

javac -cp .:jopt-simple-5.0.2.jar *.java

To run on Linux:

java -cp .:jopt-simple-5.0.2.jar GuessWho game1.config game1.chosen <player 1 type> <player 2 type>

player type = < random | binary | custom >