import java.io.*;
import java.util.*;

public class LoadData {
    @SuppressWarnings("unchecked")

    private Map<String, Map<String, Integer>> attributeToValue = new HashMap<>();
    private Map<String, Integer> hairLengthValues;
    private Map<String, Integer> glassesValues;
    private Map<String, Integer> facialHairValues;
    private Map<String, Integer> eyeColorValues;
    private Map<String, Integer> pimplesValues;
    private Map<String, Integer> hatValues;
    private Map<String, Integer> hairColorValues;
    private Map<String, Integer> noseShapeValues;
    private Map<String, Integer> faceShapeValues;

    private Map<String, String> personMap;
    private List<Map<String, String>> personList = new ArrayList<>();

    public LoadData() {
        readFile();
    }

    private void readFile() {
        File file = new File("src/files/game1.config");

        try {
            Scanner sc = new Scanner(file);

            //fetch each line from file
            while (sc.hasNextLine()) {
                //first we want to load all of the attribute, value pairs in the attributeToValue HashTable
                //so we will process the first section of the game1.configfile first

                //fetch attribute (first word) from each line
                String sCurrentAttribute = sc.next();

                //fetch values for current attribute
                String sCurrentLine = sc.nextLine();


                //fill the HashMap with all attribute, value pairs
                if (sCurrentAttribute.equals("hairLength"))
                    fillMap(sCurrentAttribute, hairLengthValues, sCurrentLine);

                else if (sCurrentAttribute.equals("glasses"))
                    fillMap(sCurrentAttribute, glassesValues, sCurrentLine);

                else if (sCurrentAttribute.equals("facialHair"))
                    fillMap(sCurrentAttribute, facialHairValues, sCurrentLine);

                else if (sCurrentAttribute.equals("eyeColor"))
                    fillMap(sCurrentAttribute, eyeColorValues, sCurrentLine);

                else if (sCurrentAttribute.equals("pimples"))
                    fillMap(sCurrentAttribute, pimplesValues, sCurrentLine);

                else if (sCurrentAttribute.equals("hat"))
                    fillMap(sCurrentAttribute, hatValues, sCurrentLine);

                else if (sCurrentAttribute.equals("hairColor"))
                    fillMap(sCurrentAttribute, hairColorValues, sCurrentLine);

                else if (sCurrentAttribute.equals("noseShape"))
                    fillMap(sCurrentAttribute, noseShapeValues, sCurrentLine);

                else if (sCurrentAttribute.equals("faceShape"))
                    fillMap(sCurrentAttribute, faceShapeValues, sCurrentLine);

                    //next we want to process the rest of the file
                    //put all Persons in a List of HashMaps
                else {
                    personMap = new HashMap<>();

                    for (int i = 0; i < 9; i++) {
                        personMap.put(sc.next(), sc.next());
                    }
                    personList.add(personMap);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fillMap(String attribute, Map<String, Integer> values, String line) {
        values = new HashMap<>();

        //separate string of values into single words and add them to the list that corresponds to the attribute
        String separate[];
        separate = line.split(" ");
        for (String s : separate) {
            if (!"".equals(s)) {
                //add values to list
                values.put(s, 0);
            }
        }
        //fill HashMap
        attributeToValue.put(attribute, values);
    }

    public Map<String, Map<String, Integer>> getAttributeToValue() {
        return attributeToValue;
    }

    public List<Map<String, String>> getPersonList() {
        return personList;
    }

    public Map<String, String> getChosenPerson(String chosenName) {
        String chosenPerson = null;

        try {
            BufferedReader assignedReader = new BufferedReader(new FileReader("src/files/game1.chosen"));

            String line;

            if ((line = assignedReader.readLine()) != null) {
                String[] fields = line.split(" ");
                if (fields.length != 2) {
                    throw new IOException("src/files/game1.chosen" + ": Misformed field line: " + line);
                }

                int i = 0;
                while (chosenPerson == null) {
                    if (fields[i].equals(chosenName)) chosenPerson = fields[i];

                    i++;
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        switch (chosenPerson) {
            case "P1":
                return personList.get(0);

            case "P2":
                return personList.get(1);

            case "P3":
                return personList.get(2);

            case "P4":
                return personList.get(3);

            case "P5":
                return personList.get(4);

            case "P6":
                return personList.get(5);

            case "P7":
                return personList.get(6);

            case "P8":
                return personList.get(7);

            case "P9":
                return personList.get(8);

            case "P10":
                return personList.get(9);
        }

        return getChosenPerson(chosenName);
    }
}