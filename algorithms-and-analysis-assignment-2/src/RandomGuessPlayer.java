import java.io.*;
import java.util.*;

/**
 * Random guessing player.
 * This player is for task B.
 * <p>
 * You may implement/extend other interfaces or classes, but ensure ultimately
 * that this class implements the Player interface (directly or indirectly).
 */
public class RandomGuessPlayer implements Player {

    private LoadData loadData = new LoadData();
    private List<Map<String, String>> personList = new ArrayList<>();
    private Map<String, String> chosenPerson = new HashMap<>();
    private String chosenName;
    private Map<String,String> guessedValues = new HashMap<>();

    /**
     * Loads the game configuration from gameFilename, and also store the chosen
     * person.
     *
     * @param gameFilename Filename of game configuration.
     * @param chosenName   Name of the chosen person for this player.
     * @throws IOException If there are IO issues with loading of gameFilename.
     *                     Note you can handle IOException within the constructor and remove
     *                     the "throws IOException" method specification, but make sure your
     *                     implementation exits gracefully if an IOException is thrown.
     */

    public RandomGuessPlayer(String gameFilename, String chosenName)
            throws IOException {
        this.chosenName = chosenName;
        //Get list of all persons and a map of the chosen person for this player
        chosenPerson = loadData.getChosenPerson(chosenName);
        personList = loadData.getPersonList();

    } // end of RandomGuessPlayer()


    public Guess guess() {
        //If the player still doesn't know the person
        if (personList.size() > 1) {
            Map<String, String> personMap;
            String attribute = null;
            String value = null;
            Random rand = new Random();
            boolean flag = true;

            //Check the guess is not unique, keep generating new random guesses
            while(flag)
            {
                flag = false;
                //Generating a random number to pick a random person from personList
                int i = rand.nextInt(personList.size());

                //Getting the attribute, value pairs for that random person
                personMap = personList.get(i);

                //Getting a random index (attribute) form personMap
                //Getting all keys of map in a List
                List<String> keys = new ArrayList<>(personMap.keySet());

                //Generating a random number to pick a random attribute
                int n = rand.nextInt(keys.size());

                //Storing the random attribute and it's value for the random person
                attribute = keys.get(n);
                value = personMap.get(keys.get(n));

                //if guess in not unique, keep looping
                if(!guessedValues.isEmpty() && value.equals(guessedValues.get(attribute)))
                    flag = true;
            }

            return new Guess(Guess.GuessType.Attribute, attribute, value);
        }
        //Else the player has one person left and it is their Person guess
        else {
            //Comparing the only person left with the list of all persons to get the persons index from list
            LoadData mLoadData = new LoadData();
            List<Map<String, String>> mPersonList = mLoadData.getPersonList();

            //Getting keys of the person left in a List
            List<String> keys = new ArrayList<>(personList.get(0).keySet());
            String opponentPerson = null;
            boolean flag;

            for (int i = 0; 0 < mPersonList.size(); i++) {
                flag = true;
                for (String s : keys) {
                    //If there is at least one value different between two persons then it is not the correct person
                    if (!personList.get(0).get(s).equals(mPersonList.get(i).get(s)))
                        flag = false;
                }
                //If flag is still true it means it is the correct person and the search is done
                if (flag) {
                    //Return the index of the correct person and exit loop
                    opponentPerson = "P" + (i + 1);
                    break;
                }
            }
            return new Guess(Guess.GuessType.Person, "", opponentPerson);
        }
    } // end of guess()


    public boolean answer(Guess currGuess) {
        if (currGuess.getType() == Guess.GuessType.Attribute) {

            //Comparing the random (attribute, value) pair the the opponent's chosen person (attribute, value) pair
            String attribute;
            String value;

            attribute = currGuess.getAttribute();
            value = currGuess.getValue();

            if (chosenPerson.get(attribute).equals(value))
                return true;
        }
        if (currGuess.getType() == Guess.GuessType.Person) {
            //If player guessed the correct person
            if (chosenName.equals(currGuess.getValue())) {
                return true;
            }
        }

        return false;
    } // end of answer()


    public boolean receiveAnswer(Guess currGuess, boolean answer) {
        //if player guessed the right person
        if (currGuess.getType() == Guess.GuessType.Person && answer)
            return true;

        //if player guessed an attribute
        eliminatePlayers(answer, currGuess.getAttribute(), currGuess.getValue());

        return false;
    } // end of receiveAnswer()

    private void eliminatePlayers(boolean answer, String attribute, String value) {
        Map<String, String> map;
        //if answer is true, eliminate all players who do not have the (attribute, value) pair
        if (answer) {
            for (int i = personList.size() - 1; i >= 0; i--) {
                map = personList.get(i);

                if (!map.get(attribute).equals(value))
                    personList.remove(i);
            }
            guessedValues.put(attribute, value);
        }
        //if answer is false, eliminate all players who have the (attribute, value) pair
        if (!answer) {
            for (int i = personList.size() - 1; i >= 0; i--) {
                map = personList.get(i);

                if (map.get(attribute).equals(value))
                    personList.remove(i);
            }
        }
    }


} // end of class RandomGuessPlayer
