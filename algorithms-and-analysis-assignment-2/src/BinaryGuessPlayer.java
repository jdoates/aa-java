import java.io.*;
import java.util.Map;
import java.util.*;
import java.util.Map.Entry;

/**
 * Binary-search based guessing player.
 * This player is for task C.
 * <p>
 * You may implement/extend other interfaces or classes, but ensure ultimately
 * that this class implements the Player interface (directly or indirectly).
 */
public class BinaryGuessPlayer implements Player {
    private Map<String, Map<String, Integer>> attributeToValue = new HashMap<>();
    private LoadData loadData = new LoadData();
    private List<Map<String, String>> personList = new ArrayList<>();
    private Map chosenPerson = new HashMap<>();
    private String chosenName;


    /**
     * Loads the game configuration from gameFilename, and also store the chosen
     * person.
     *
     * @param gameFilename Filename of game configuration.
     * @param chosenName   Name of the chosen person for this player.
     * @throws IOException If there are IO issues with loading of gameFilename.
     *                     Note you can handle IOException within the constructor and remove
     *                     the "throws IOException" method specification, but make sure your
     *                     implementation exits gracefully if an IOException is thrown.
     */
    public BinaryGuessPlayer(String gameFilename, String chosenName)
            throws IOException {
        LoadData mLoadData = new LoadData();

        attributeToValue = mLoadData.getAttributeToValue();
        personList = mLoadData.getPersonList();

        //Load Everyones data into lists for each of their data values

        //update the list.

        //read file

    } // end of BinaryGuessPlayer()

    /******************************************************************************************************
     * This Binary Guess Firstly Gets all the people data remaining, and makes a frequency list with it
     * and then goes through that frequency list and picks the attribute with the closest value to
     * half the remaining people, this should half (or close to) the amount of people each time
     * a guess is made, if there is only one remaining the person should be gussed.
     ******************************************************************************************************/

    public Guess guess() {

        float best_Ratio = 0;

        if (personList.size() > 1) {

            //Goes through the Person Map getting their Attributes and Values
            for (Map<String, String> map : personList) {

                List<String> personListKeys = new ArrayList<>(map.keySet());

                for (String personListKey : personListKeys) {
                    String personValue = map.get(personListKey);

                    //Goes through the Data Tree getting the keys and tries to match with the People attribute key
                    List<String> attributeToValueKeys = new ArrayList<>(attributeToValue.keySet());

                    for (String mapKey : attributeToValueKeys) {
                        {
                            Map<String, Integer> attributeToValue_value = attributeToValue.get(mapKey);

                            List<String> attributeToValueKeys_keys = new ArrayList<>(attributeToValue_value.keySet());

                            for (String attributeToValueKeys_key : attributeToValueKeys_keys) {

                                int d_value = attributeToValue_value.get(attributeToValueKeys_key);
                                int frequency = 0;

                                //If matched it then goes through the possiblities for that attribute and compares
                                if (attributeToValueKeys_key.equals(personValue)) {
                                    if (personValue.equals(attributeToValueKeys_key)) {
                                        frequency++;
                                        attributeToValue_value.put(attributeToValueKeys_key, frequency);
                                        attributeToValue.put(mapKey, attributeToValue_value);
                                    }
                                }
                            }
                        }
                    }
                }
                //iterate through the frequency list and then compares
                //each value with another and the best ratio for the guess is recorded and used.
                List<String> attributeToValueKeys = new ArrayList<>(attributeToValue.keySet());

                for (String mapKey : attributeToValueKeys) {
                    {
                        Map<String, Integer> attributeToValue_value = attributeToValue.get(mapKey);

                        List<String> attributeToValueKeys_keys = new ArrayList<>(attributeToValue_value.keySet());

                        for (String attributeToValueKeys_key : attributeToValueKeys_keys) {

                            int frequency = attributeToValue_value.get(attributeToValueKeys_key);

                            check_Frequency(frequency, mapKey, attributeToValueKeys_key, best_Ratio);
                        }
                    }
                }
            }
        } else {
            // Guess the only remining Person
            return new Guess(Guess.GuessType.Person, "", "Person");
        }
        return new Guess(Guess.GuessType.Person, "", "Person");
    } // end of guess()

    /******************************************************************************************************
     * This Checks the best Frequency for each of the Attributes,
     * if it has a better ratio then it makes the best guess that answer
     * once the searching has ended the best guess is then sent
     ******************************************************************************************************/


    private void check_Frequency(int frequency, String attribute_Value, String attribute_Key, float best_Ratio) {
        float ratio = frequency / personList.size();

        ratio = (float) (ratio - 0.5);

        if (Math.abs(ratio) < best_Ratio) {
            best_Ratio = Math.abs(ratio);
            new Guess(Guess.GuessType.Attribute, attribute_Key, attribute_Value);
        }
    }

    public boolean answer(Guess currGuess) {

        if (currGuess.getType() == Guess.GuessType.Attribute) {

            String attribute;
            String value;

            attribute = currGuess.getAttribute();
            value = currGuess.getValue();

            if (chosenPerson.get(attribute).equals(value))
                return true;
        }
        if (currGuess.getType() == Guess.GuessType.Person) {
            if (chosenName.equals(currGuess.getValue())) {
                return true;
            }
        }

        return false;
        //Sent from other player returns if the characistic is true/false
    } // end of answer()


    public boolean receiveAnswer(Guess currGuess, boolean answer) {


        if (currGuess.getType() == Guess.GuessType.Person && answer)
            return true;

        eliminatePlayers(answer, currGuess.getAttribute(), currGuess.getValue());

        return false;
    } // end of receiveAnswer()

    private void eliminatePlayers(boolean answer, String attribute, String value) {
        Map<String, String> map;
        if (answer) {
            for (int i = 0; i < personList.size(); i++) {
                map = personList.get(i);

                if (!map.get(attribute).equals(value))
                    personList.remove(i);
            }
        }
        if (!answer) {
            for (int i = 0; i < personList.size(); i++) {
                map = personList.get(i);

                if (map.get(attribute).equals(value))
                    personList.remove(i);
            }
        }
    } // end of receiveAnswer()
} // end of class BinaryGuessPlayer
